package ejercicio.distribucioncargas;

import java.util.Scanner;

public class Distribucion
{   
    private int cont = 0;
    private Carga [] dis;
    public Distribucion(){
        dis = new Carga [100];
        cont = 0;
    }

    public int getCont(){
        return cont;
    }

    public boolean distribucionVacia(){
        return cont == 0;
    }

    public boolean distribucionLlena(){
        return cont == dis.length;
    }

    public int pideTipo(){
        Scanner teclado = new Scanner(System.in);
        System.out.println("¿Tipo de coordenadas? (cartesiana(1)/cilindrica(2)/esferica(3)): ");
        int tipo = teclado.nextInt();
        return tipo;
    }

    public double pideValor(){
        Scanner teclado = new Scanner(System.in);
        System.out.println("Introduce valor de la carga: ");
        double val = teclado.nextDouble();
        return val;
    }

    public Punto pasaACartesianas(Punto p){
        double x = 0.0;
        double y = 0.0;
        double z = 0.0;
        double a = p.getA();
        double b = p.getB();
        double c = p.getC();
        int t = p.getTipo();

        if( t == 1){
            x = a;
            y = b;
            z = c;
        }
        else if( t == 2){
            x = b*Math.cos(a);
            y = b*Math.sin(a);
            z = c;
        }
        else if( t == 3){
            x = c*Math.sin(a)*Math.cos(b);
            y = c*Math.sin(a)*Math.sin(b);
            z = c*Math.cos(a);
        }
        Punto punto = new Punto(1,x,y,z);
        return punto;
    }

    public Carga pasaCartesianas(Carga c1){
        double x = 0.0;
        double y = 0.0;
        double z = 0.0;
        double a = c1.getPunto().getA();
        double b = c1.getPunto().getB();
        double c = c1.getPunto().getC();

        if( c1.getPunto().getTipo() == 1){
            x = a;
            y = b;
            z = c;
        }
        else if( c1.getPunto().getTipo() == 2){
            x = b*Math.cos(a);
            y = b*Math.sin(a);
            z = c;
        }
        else if(c1.getPunto().getTipo() == 3){
            x = c*Math.sin(a)*Math.cos(b);
            y = c*Math.sin(a)*Math.sin(b);
            z = c*Math.cos(a);
        }

        Punto punto = new Punto(1,x,y,z);
        Carga aux = new Carga(c1.getValor(),punto);
        return aux;
    }

    public Carga pasaCilindricas(Carga c1){
        double x = 0.0;
        double y = 0.0;
        double z = 0.0;
        double a = c1.getPunto().getA();
        double b = c1.getPunto().getB();
        double c = c1.getPunto().getC();

        if( c1.getPunto().getTipo() == 1){
            x = Math.sqrt((a*a)+(y*y));
            y = Math.atan(b/a);
            z = c;
        }
        else if( c1.getPunto().getTipo() == 2){
            x = a;
            y = b;
            z = c;
        }
        else if(c1.getPunto().getTipo() == 3){
            Punto aux1 = pasaACartesianas(c1.getPunto());
            x = Math.sqrt(Math.sqrt(Math.pow(aux1.getA(),2)+Math.pow(aux1.getB(),2)));
            y = Math.atan(aux1.getB()/aux1.getA());
            z = c;
        }

        Punto punto = new Punto(2,x,y,z);
        Carga aux = new Carga(c1.getValor(),punto);
        return aux;
    }

    public Carga pasaEsfericas(Carga c1){
        double x = 0.0;
        double y = 0.0;
        double z = 0.0;
        double a = c1.getPunto().getA();
        double b = c1.getPunto().getB();
        double c = c1.getPunto().getC();

        if( c1.getPunto().getTipo() == 1){
            x = Math.sqrt((a*a)+(b*b)+(c*c));
            y = Math.acos(c/x);
            z = Math.atan(b/a);
        }
        else if( c1.getPunto().getTipo() == 2){
            Punto aux1 = pasaACartesianas(c1.getPunto());
            x = Math.sqrt(Math.pow(aux1.getA(),2)+Math.pow(aux1.getB(),2)+Math.pow(aux1.getC(),2));
            y = Math.acos(aux1.getC()/x);
            z = Math.atan(b/a);
        }
        else if(c1.getPunto().getTipo() == 3){
            x = a;
            y = b;
            z = c;
        }

        Punto punto = new Punto(3,x,y,z);
        Carga aux = new Carga(c1.getValor(),punto);
        return aux;
    }

    public int busqueda(Carga c1){
        int indice = -1;
        Carga aux1 = pasaCartesianas(c1);
        int i = 0;
        int s = 0;
        double l = 0;
        int m;
        if(!distribucionVacia()){
            i=0;
            s = cont - 1;
            Carga aux3 = pasaCartesianas(dis[i]);
            while(i!= s){
                l = ((i + s) /2);
                m = (int)(l);
                Carga aux2 = pasaCartesianas(dis[m]);
                Carga aux4 = pasaCartesianas(dis[s]);
                if(aux2.getPunto().getA() == aux1.getPunto().getA() && aux2.getPunto().getB() == aux1.getPunto().getB()  && aux2.getPunto().getC() == aux1.getPunto().getC()  && aux2.getValor() == c1.getValor()){
                    indice = m;
                    i = s;
                }
                else{
                    if(aux3.getPunto().getA() == aux1.getPunto().getA()  && aux3.getPunto().getB() == aux1.getPunto().getB() && aux3.getPunto().getC() == aux1.getPunto().getC()  && aux3.getValor() == c1.getValor()){
                        indice = i;
                        i = s;
                    }
                    else{
                        if(aux4.getPunto().getA() == aux1.getPunto().getA() && aux4.getPunto().getB() == aux1.getPunto().getB() && aux4.getPunto().getC() == aux1.getPunto().getC()  && aux4.getValor() == c1.getValor()){
                            indice = s;
                            i = s;
                        }
                        else{
                            if(cargaOrdenada(aux2, aux1)){
                                i = m + 1;
                            }
                            else s = m;
                        }
                    }
                }
            }
            if(aux3.getPunto().getA() == aux1.getPunto().getA()  && aux3.getPunto().getB() == aux1.getPunto().getB() && aux3.getPunto().getC() == aux1.getPunto().getC()  && aux3.getValor() == c1.getValor())
                indice = i;
        }
        return indice;
    }

    public void annadeCargaOrdenada(Carga c){
        if(!distribucionLlena()){
            int h = cont - 1;
            while(h >= 0 && !cargaOrdenada(dis[h],c)){
                dis[h+1] = dis[h];
                h --;
            }
            dis [h+1] = c;
            cont ++;
        }
    }

    public void eliminaCarga(Carga c){
        if(!distribucionVacia()){
            int m = busqueda(c);
            for(int i = m; i < cont ; i++){
                dis[i] = dis [i+1];
            }
            dis[cont-1]=null;
            cont --;
        }
    }

    public void eliminaCargasMenores(){
        double val = pideValor();
        if(!distribucionVacia()){
            for(int i = 0; i < cont ; i++){
                if(dis[i].getValor()<val){
                    for(int j = i;j< cont; j++){
                        dis[j] = dis [j+1];
                    }
                    dis[cont -1]=null;
                    cont --;
                }
            }
        }
    }

    public boolean cargaOrdenada(Carga c1 , Carga c2){
        boolean ordenados = false;
        Carga aux1 = pasaCartesianas(c1);
        Carga aux2 = pasaCartesianas(c2);

        if(aux1.getPunto().getA() < aux2.getPunto().getA()){
            ordenados=true;
        }
        else{
            if(aux1.getPunto().getA() == aux2.getPunto().getA()){
                if(aux1.getPunto().getB() < aux2.getPunto().getB()){
                    ordenados= true;
                }
                else{
                    if(aux1.getPunto().getB() == aux2.getPunto().getB()){
                        if(aux1.getPunto().getC() < aux2.getPunto().getC()){
                            ordenados= true;
                        }
                        else{
                            if(aux1.getPunto().getC() == aux2.getPunto().getC()){
                                if(aux1.getValor() < aux2.getValor()){
                                    ordenados= true;
                                }
                                else{
                                    if(aux1.getValor() == aux2.getValor()){
                                        ordenados = true;
                                    }
                                    else{
                                        ordenados=false;
                                    }   
                                }
                            }
                            else{
                                ordenados = false;
                            }
                        }   
                    }   
                    else{
                        ordenados=false;
                    }
                }
            }
            else{
                ordenados=false;
            }
        }
        return ordenados;
    }

    public void modificarCarga(){
        double val = pideValor();
        Punto p = daPunto();
        Carga c = new Carga(val,p);
        int posi = busqueda(c);
        if(posi >= 0){
            Carga aux = dis[posi];
            eliminaCarga(c);
            int opcion = -1;
            do{
                System.out.println(" ");
                System.out.println("¿Que desea modificar? ");
                System.out.println("1. Modificar Posicion ");
                System.out.println("2. Modificar Valor ");
                System.out.println("3. Salir ");
                opcion = guardaEntero();
                switch(opcion){
                    case 1: int t = pideTipo();
                            aux.getPunto().setA(pideA(t));
                            aux.getPunto().setB(pideB(t));
                            aux.getPunto().setC(pideC(t));
                            annadeCargaOrdenada(aux);break;
                    case 2: double valor = pideValor();
                            aux.setValor(val);break;
                    case 3:break;
                    default:System.out.println("La opcion seleccionada no es correcta");
                }
            }
            while(opcion != 3);
        }
        else
            System.out.println("No existe la carga ");
    }
    
    public int guardaEntero(){
        Scanner teclado = new Scanner(System.in);
        int n = -1;
        String noValido;
        try{
            n = teclado.nextInt();
        }
        catch(Exception e){
            noValido =teclado.nextLine();
        }
        return n;
    }

    public boolean comprobarCargaPunto(Punto p){
        boolean existe = false;
        Punto aux = pasaACartesianas(p);
        double x = aux.getA();
        double y = aux.getB();
        double z = aux.getC();
        Punto aux1 = null;
        if(!distribucionVacia()){
            int i = 0;
            while(i<cont && existe == false){
                aux1 = pasaACartesianas(dis[i].getPunto());
                double x1 = aux1.getA();
                double y1 = aux1.getB();
                double z1 = aux1.getC();
                if(x == x1 && y == y1 && z == z1){
                    existe = true;
                }
                else
                    i++;
            }
        }
        return existe;
    }
    
    public boolean compruebaPunto(){
        Punto p = daPunto();
        boolean comprobado = false;
        comprobado = comprobarCargaPunto(p);
        return comprobado;
    }
    
    public boolean compruebaCargaDistancia(Punto p, double dist){
        boolean existe = false;
        Punto aux1 = pasaACartesianas(p);
        Punto aux2 = null;
        double x = aux1.getA();
        double y = aux1.getB();
        double z = aux1.getC();
        if(!distribucionVacia()){
            int i = 0;
            while(i<cont && existe == false){
                aux2 = pasaACartesianas(dis[i].getPunto());
                double x1 = aux2.getA();
                double y1 = aux2.getB();
                double z1 = aux2.getC();
                double dist1 = Math.sqrt(((x1-x)*(x1-x))+((y1-y)*(y1-y))+((z1-z)*(z1-z)));
                if(dist1 == dist){
                    existe = true;
                }
                else
                    i++;
            }
        }
        return existe;
    }
    
    public boolean compruebaDistancia(){
        Scanner teclado = new Scanner(System.in);
        Punto p = daPunto();
        System.out.println("Introduce una distancia: ");
        double dist = teclado.nextDouble();
        boolean comprobado = false;
        comprobado = compruebaCargaDistancia(p,dist);
        return comprobado;
    }
    
    public Distribucion listaCargasValorMayor(double val){
        int n = 0;
        Distribucion d = new Distribucion();
        if(!distribucionLlena()){
            for(int i = 0; i < cont ; i++){
                if(dis[i].getValor()>val){
                    d.dis[n] = dis [i];
                    n++;
                }
            }
        }
        d.cont = n;
        return d;
    }

    public Punto campoElectrico(){
        Punto punto = daPunto();
        double a = 0,b = 0,c = 0;
        Punto p = pasaACartesianas(punto);
        double x = p.getA();
        double y = p.getB();
        double z = p.getC();
        if(!distribucionVacia()){
            for(int i = 0;i < cont; i++){
                Punto aux = pasaACartesianas(dis[i].getPunto());
                double x1 = aux.getA();
                double y1 = aux.getB();
                double z1 = aux.getC();
                double modulo = Math.sqrt(((x1-x)*(x1-x))+((y1-y)*(y1-y))+((z1-z)*(z1-z)));
                a += ((x1-x)/modulo)*(9*(Math.pow(10,9))*(dis[i].getValor()))/(modulo*modulo);
                b += ((y1-y)/modulo)*(9*(Math.pow(10,9))*(dis[i].getValor()))/(modulo*modulo);
                c += ((z1-z)/modulo)*(9*(Math.pow(10,9))*(dis[i].getValor()))/(modulo*modulo);
            }
        }
        Punto campo = new Punto(1,a,b,c);
        return campo;
    }

    public double  potencialElectrico(){
        Punto punto = daPunto();
        double poten = 0,sum = 0;
        Punto p = pasaACartesianas(punto);
        double x = p.getA();
        double y = p.getB();
        double z = p.getC();
        if(!distribucionVacia()){
            for(int i = 0;i < cont; i++){
                Punto aux = pasaACartesianas(dis[i].getPunto());
                double x1 = aux.getA();
                double y1 = aux.getB();
                double z1 = aux.getC();
                double modulo = Math.sqrt(((x1-x)*(x1-x))+((y1-y)*(y1-y))+((z1-z)*(z1-z)));
                sum += (dis[i].getValor())/(modulo);
            }
        }
        poten = 9*(Math.pow(10,9))*(sum);
        return poten; 
    }

    public double difPotencial(){
        double dif = potencialElectrico()-potencialElectrico();
        return dif;
    }

    public void guardaEnMemoria(){
        Distribucion memoria = new Distribucion();
        for(int i=0; i<cont;i++){
            memoria.annadeCargaOrdenada(dis[i]);
        }
    }

    public void cargaMemoria(Distribucion memoria){
        for(int i=0; i<cont; i++){
            eliminaCarga(dis[i]);
        }

        for(int i=0; i<cont; i++){
            annadeCargaOrdenada(memoria.dis[i]);
        }

    }

    public void fusionMemoria(Distribucion memoria){
        if(100-(cont-1)>=(memoria.getCont()-1)){
            for(int i=0; i<cont; i++){
                annadeCargaOrdenada(memoria.dis[i]);
            }
        }
    }

    public void escribirDistribucion(){
        String t = "";
        for(int i = 0; i<cont;i++){
            if(dis[i].getPunto().getTipo() == 1){
                t = "Cartesianas";
            }else if(dis[i].getPunto().getTipo() == 2){
                t = "Cilindricas";
            }else if(dis[i].getPunto().getTipo() == 3){
                t = "Esfericas";
            }
            System.out.println("Valor: "+dis[i].getValor()+" Coordenadas: ("+dis[i].getPunto().getA()+","+dis[i].getPunto().getB()+","+dis[i].getPunto().getC()+") en: "+t);
        }
    }

    public double pideA(int tipo){
        Scanner teclado = new Scanner(System.in);
        double a = 0.0;

        if( tipo == 1){
            System.out.println("Introduce coordenada x:");
            a = teclado.nextDouble();

        }
        else if( tipo == 2){
            System.out.println("Introduce un angulo alfa:");
            a = teclado.nextDouble();

        }
        else if( tipo == 3){
            System.out.println("Introduce un angulo alfa:");
            a = teclado.nextDouble();

        }
        return a;
    }

    private double pideB(int tipo){
        Scanner teclado = new Scanner(System.in);
        double b = 0.0;

        if( tipo == 1){
            System.out.println("Introduce coordenada y:");
            b = teclado.nextDouble();

        }
        else if( tipo == 2){
            System.out.println("Introduce una distancia p:");
            b = teclado.nextDouble();

        }
        else if( tipo == 3){
            System.out.println("Introduce un angulo beta:");
            b = teclado.nextDouble();

        }
        return b;
    }

    public double pideC(int tipo){
        Scanner teclado = new Scanner(System.in);
        double c = 0.0;

        if( tipo == 1){
            System.out.println("Introduce coordenada z:");
            c = teclado.nextDouble();

        }
        else if( tipo == 2){
            System.out.println("Introduce un vector z:");
            c = teclado.nextDouble();

        }
        else if( tipo == 3){
            c = teclado.nextDouble();

        }
        return c;
    }

    public Punto daPunto(){
        int t = pideTipo();
        double a = pideA(t);
        double b = pideB(t);
        double c = pideC(t);
        Punto p = new Punto(t,a,b,c);
        return p;
    }

    public Carga daCarga(){
        Punto p = daPunto();
        double valor = pideValor();
        Carga c = new Carga(valor,p);
        return c;
    }

    public void nuevaCarga(){
        Carga c = daCarga();
        if(!distribucionLlena()){
            int h = cont - 1;
            while(h >= 0 && !cargaOrdenada(dis[h],c)){
                dis[h+1] = dis[h];
                h --;
            }
            dis [h+1] = c;
            cont ++;
        }
    }

    public void eliminaCarga(){
        Carga c = daCarga();
        if(!distribucionVacia()){
            int m = busqueda(c);
            for(int i = m; i < cont ; i++){
                dis[i] = dis [i+1];
            }
            dis[cont-1]=null;
            cont --;
        }
    }

}

