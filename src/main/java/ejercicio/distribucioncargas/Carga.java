package ejercicio.distribucioncargas;

public class Carga
{
    private Punto punto;
    private double valor;
    public Carga(double val,Punto p){
        valor = val;
        punto = p;
    }
    
    public double getValor(){
        return valor;
    }
    
     public Punto getPunto(){
        return punto;
    }
    
    public void setValor(double v){
        valor = v;
    }

    public void setPunto(Punto p){
        punto = p;
    }
}
