package ejercicio.distribucioncargas;

import java.util.*;

public class Menu{
    public static void main(String[]args){
        int opcion;
        Distribucion dis = new Distribucion();
        Distribucion memoria = new Distribucion();
        Scanner teclado = new Scanner(System.in);
        System.out.println("Elija una opcion de entre las siguientes:");
        System.out.println(" ");
        System.out.println("1. Crear una nueva distribucion.");
        System.out.println("2. Mostrar distribucion.");
        System.out.println("3. Añadir una nueva carga a la distribucion.");
        System.out.println("4. Quitar una carga de la distribucion.");
        System.out.println("5. Quitar  las cargas cuyo valor absoluto sea menor a uno determinado.");
        System.out.println("6. Modificar una carga de la distribucion.");
        System.out.println("7. Comprobación si existe una carga en la distribucion con unas coordenadas concretas.");
        System.out.println("8. Comprobación si existe una carga en la distribucion a una distancia concreta del origen de coordenadas.");
        System.out.println("9. Listado de las de las cargas cuyo valor es superior a uno dado.");
        System.out.println("10. Calcular el campo electrico de la distribucion en un punto dado.");
        System.out.println("11. Calcular el potencial electrico de la distribucion en un punto dado.");
        System.out.println("12. Calcular la diferencia de potencial entre dos puntos.");
        System.out.println("13. Guardar la distribucion en memoria.");
        System.out.println("14. Cargar memoria en la distribucion actual.");
        System.out.println("15. Fusionar la distribucion de trabajo con la de memoria.");
        System.out.println("16. Salir del programa.");
        System.out.println(" ");
       
        do{
            System.out.println("Introduzca operacion a realizar:");
            opcion = teclado.nextInt();
            switch(opcion){
                case 1: dis = new Distribucion();
                System.out.println("Distribucion creada");break;
                case 2: if(dis.distribucionVacia()){
                    System.out.println("La distribucion esta vacia");
                }else {dis.escribirDistribucion();break;
                }
                case 3: 
                int n = -1;
                while(n < 0 || n > 100){
                    System.out.println("¿Cuántas cargas quiere añadir a la distribucion");
                    n = teclado.nextInt();
                    for( int i = 1; i <= n; i++)
                        dis.nuevaCarga();
                }
                break;
                case 4: dis.eliminaCarga(); break;
                case 5: dis.eliminaCargasMenores(); break;
                case 6: dis.modificarCarga(); break;
                case 7: boolean comprobado = (dis.compruebaPunto());
                if(comprobado == true){
                    System.out.println("La carga existe");
                }else 
                    System.out.println("La carga no existe");
                break;

                case 8: boolean comprobado1 = (dis.compruebaDistancia());
                if(comprobado1 == true){
                    System.out.println("La carga existe");
                }else 
                    System.out.println("La carga no existe");
                break;
                case 9: System.out.println("Introduce un valor: ");
                double val = teclado.nextDouble();
                Distribucion dis1 = (dis.listaCargasValorMayor(val));
                dis1.escribirDistribucion();
                break;
                case 10:System.out.println("Introduce un punto para calcular el campo electrico:" );
                Punto p2 = dis.campoElectrico();
                System.out.println("El campo electrico es( "+p2.getA()+"i ,"+p2.getB()+"j ,"+p2.getC()+"k)N/C");
                break;
                case 11: System.out.println("Introduce un punto para calcular el potencial electrico:" );
                double pot = dis.potencialElectrico();
                System.out.println("El potencial electrico en el punto es: "+pot);
                break;
                case 12:double dif = dis.difPotencial();
                System.out.println("La diferencia de potencial es: "+dif);
                break;
                case 13:dis.guardaEnMemoria();
                break;
                case 14:dis.cargaMemoria(memoria);
                break;
                case 15:dis.fusionMemoria(memoria);
                break;
                case 16:;break;
                default: System.out.println("La opción seleccionada no es correcta.");
            }
        }
        while(opcion != 16);
    }
}

