package ejercicio.distribucioncargas;

public class Punto{
    private double a,b,c;
    private int tipo =0;
    public Punto(int t,double a1,double b1,double c1){
        a = a1;
        b = b1;
        c = c1;
        tipo = t;
    }
    
    public int getTipo(){
        return tipo;
    }
    
    public double getA(){
        return a;
    }

    public double getB(){
        return b;
    }

    public double getC(){
        return c;
    }
    
    public void setTipo(int t){
        tipo = t;
    }
    
    public void setA(double x){
        a = x;
    }

    public void setB(double y){
        b = y;
    }

    public void setC(double z){
        c = z;
    }

    
}
